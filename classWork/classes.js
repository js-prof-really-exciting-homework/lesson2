/*
  Задание:

    I. Написать класс Post который будет уметь:

      1. Конструктор:
          title
          image
          description
          likes

      2. Методы
          render -> отрисовать элемент в ленте
          likePost -> увеличивает счетчик постов
          + Методы для изменения title, image, description
          + бонус. Сделать получение и изменение через set и get

    II. Написать класс Advertisment который будет экстендится от класа Post
        но помимо будет иметь другой шаблон вывода, а так же метод для покупки обьекта

        buyItem -> выведет сообщение что вы купили обьект

    III.  Сгенерировать ленту из всех постов что вы добавили в систему.
          Каждый третий пост должен быть рекламным

    <div class="post">
      <div class="post__title">Some post</div>
      <div class="post__image">
        <img src="https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png"/>
      </div>
      <div class="post__description"></div>
      <div class="post__footer">
        <button class="post__like">Like!</button>
      </div>
    </div>




*/

const posts = [
  {
    title: 'First super post',
    image: 'https://cdn1.iconfinder.com/data/icons/UltraBuuf/512/Red_Chin_Iron_Man.png',
    description: 'This is Iron man!'
  },

  {
    title: 'Jocker?',
    image: 'https://cdn1.iconfinder.com/data/icons/UltraBuuf/512/Serious.png',
    description: 'Somebody looks like Jocker'
  },

  {
    title: 'Capt. America',
    image: 'https://cdn1.iconfinder.com/data/icons/UltraBuuf/512/capo.png',
    description: 'The first avenger'
  },

  {
    title: 'Hellboy',
    image: 'https://cdn1.iconfinder.com/data/icons/UltraBuuf/512/Hellboy.png',
    description: 'Rather funny dude'
  },

  {
    title: 'Mr. Smith',
    image: 'https://cdn1.iconfinder.com/data/icons/UltraBuuf/512/Smith.png',
    description: 'Everybody likes me. Smith isn\'t it?'
  },

  {
    title: 'Woody',
    image: 'https://cdn1.iconfinder.com/data/icons/UltraBuuf/512/Woody.png',
    description: 'Really? What am I doing here? O_o'
  }
];

const ads = [
  {
    title: 'Buy this awesome stuff!',
    image: 'https://files.brightside.me/files/news/part_40/403360/preview-17272110-650x341-98-1511723586.jpg',
    description: 'You always wanted to keep butter in iphone!',
    price: 100
  },

  {
    title: 'How did you live your entire life without this... thing?',
    image: 'https://i.ytimg.com/vi/m4PDpbqBenQ/hqdefault.jpg',
    description: 'This is a brand new life saving completely necessary thing...',
    price: 123098
  },

  {
    title: 'You really-really need this!!!',
    image: 'https://oddstuffmagazine.com/wp-content/uploads/2012/11/things-013.jpg',
    description: 'Tired to put your socks on? Try this!',
    price: 98099009
  },
]

const feed = document.getElementById('posts_feed');
let AdsCounter = 0;

class Post{
  constructor(postObj){
    let {title, image, description} = postObj;
    this._title = title;
    this._image = image;
    this._description = description;
    this.likes = 0;
    this.render = this.render.bind( this );
  }

  likePost(){
    this.likes++
  }

  render(){
    let div = document.createElement('div');
    div.classList.add('post');
    let postBody =
                    `
                    <div class="post">
                      <div class="post__title">${this._title}</div>
                      <div class="post__image">
                        <img src="${this._image}"/>
                      </div>
                      <div class="post__description">${this._description}</div>
                      <div class="post__footer">
                        <button class="post__like">Like! (${this.likes})</button>
                      </div>
                    </div>
                    `
    div.innerHTML = postBody;
    let btn = div.querySelector(`.post__like`);

    //я тут не стал заморачиваться с id чтобы вынести логику в метод likePost()
    btn.addEventListener('click', () =>{
      this.likePost();
      btn.innerHTML = `Like! (${this.likes})`;
    } )

    return div;
  }

  get title() {
    return this._title = this._title;
  }

  set title(newTitle){
    if (newTitle.length <= 10 && newTitle.length > 0){
      this._title = newTitle;
      // this.render();
    } else {
      throw new Error(`That's an error! Title can't be more than 10 symbols`);
    }
    
    
  }

  get image() {
    return this._image = `<img src="${this._image}">`;
  }
  
  set image(newImg){    
    this._image = newImg;
    // this.render();
  }

  get description() {
    return this._description = `Your result is: ${this._description}`;
  }
  
  set description (newDescr){
    this._description = `<p style="color: red;">${newDescr}</p>`;
    // this.render();
  }
}

class Advertisment extends Post{
  constructor( postObj ){
    let {price} = postObj;
    super( postObj );
    this._price = price;
    this.buy = this.buy.bind( this );
  }

  get price(){
    return `$${this._price}`
  }

  render(){    
    let div = document.createElement('div');
    div.classList.add('post');
    let postBody =
                    `
                    <div class="post Advertisment">
                      <div class="post__title advert">${this._title}, Price: ${this.price}</div>
                      <div class="post__image">
                        <img src="${this._image}"/>
                      </div>
                      <div class="post__description">${this._description}</div>
                      <div class="post__footer">
                        <button class="post__buy" data-buy="${this._title}">Buy!</button>
                      </div>
                    </div>
                    `
    div.innerHTML = postBody;
    let btn = div.querySelector(`.post__buy`);
        btn.addEventListener('click', this.buy);

    return div;
  }

  buy(){
    let popup = document.getElementById('popup');
    let popupContent = document.createElement('div');
    let template =
      `
      Congratulations, you've bought ${this._title} for the price: ${this.price}
      `
    popupContent.classList.add('popupContent');
    popupContent.innerHTML = template;
    popup.appendChild(popupContent);
    popup.style.opacity = '1';
    popup.style.zIndex = '99';

    setTimeout(()=>{
      popup.style.opacity = '0';
      popup.style.zIndex = '-99';      
    },2000)
    setTimeout(()=> popupContent.remove(), 4000)
  }
  
}



//TESTS
// let awesomePost = new Post('тайтл', 'https://www.motorcyclecruiser.com/sites/motorcyclecruiser.com/files/styles/2000_1x_/public/images/2017/12/yamaha-warrior-hero_0.jpg', 'DESCR');

// // awesomePost.title= '1234567891011';
// awesomePost.title= 'Cool title';
// awesomePost.description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc arcu nulla, euismod ac arcu non, sollicitudin tristique odio. Morbi ornare laoreet tristique.';
// awesomePost.render();
// console.log(awesomePost.image);
// console.log(awesomePost.title);
// let newAd = new Advertisment('Super Bike', 'https://i.ytimg.com/vi/Q6xybdaV3xs/maxresdefault.jpg', 'TEXT', 20);
// newAd.render();

// let newPost = new Post ({title: 'Woody',image: 'https://cdn1.iconfinder.com/data/icons/UltraBuuf/512/Woody.png',description: 'Really? What am I doing here? O_o'});
// console.log(newPost);
// newPost.render();

posts.map( (item, key) => {
  if ( key != 0 && key % 2 === 0 ){      
      let advert = new Advertisment(ads[AdsCounter]);
      feed.appendChild( advert.render() );
      console.log(ads[AdsCounter])
      AdsCounter++
  }
  let post = new Post(item);
  feed.appendChild( post.render() );
  if ( key === posts.length-1 ){
    let advert = new Advertisment(ads[AdsCounter]);
    feed.appendChild( advert.render() );
  }
});