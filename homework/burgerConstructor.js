

  /*

    Задание:

      1. Создать конструктор бургеров на прототипах, которая добавляет наш бургер в массив меню

      Дожно выглядеть так:
      new burger('Hamburger',[ ...Массив с ингредиентами ] , 20);

      Моделька для бургера:
      {
        cookingTime: 0,     // Время на готовку
        showComposition: function(){
          let {composition, name} = this;
          let compositionLength = composition.length;
          console.log(compositionLength);
          if( compositionLength !== 0){
            composition.map( function( item ){
                console.log( 'Состав бургера', name, item );
            })
          }
        }
      }

      Результатом конструктора нужно вывести массив меню c добавленными элементами.
      // menu: [ {name: "", composition: [], cookingTime:""},  {name: "", composition: [], cookingTime:""}]

        2. Создать конструктор заказов

        Моделька:
        {
          id: "",
          orderNumber: "",
          orderBurder: "",
          orderException: "",
          orderAvailability: ""
        }

          Заказ может быть 3х типов:
            1. В котором указано название бургера
              new Order('Hamburger'); -> Order 1. Бургер Hamburger, будет готов через 10 минут.
            2. В котором указано что должно быть в бургере, ищете в массиве Menu подходящий вариант
              new Order('', 'has', 'Название ингредиента') -> Order 2. Бургер Чизбургер, с сыром, будет готов через 5 минут.
            3. В котором указано чего не должно быть
              new Order('', 'except', 'Название ингредиента') ->


            Каждый их которых должен вернуть статус:
            "Заказ 1: Бургер ${Название}, будет готов через ${Время}

            Если бургера по условиям заказа не найдено предлагать случайный бургер из меню:
              new Order('', 'except', 'Булка') -> К сожалению, такого бургера у нас нет, можете попробовать "Чизбургер"
              Можно например спрашивать через Confirm подходит ли такой вариант, если да - оформлять заказ
              Если нет, предложить еще вариант из меню.

        3. Протестировать программу.
          1. Вначале добавляем наши бургеры в меню (3-4 шт);
          2. Проверяем работу прототипного наследования вызывая метод showComposition на обьект бургера с меню
          3. Формируем 3 заказа

        Бонусные задания:
        4. Добавлять в исключения\пожелания можно несколько ингредиентов
        5. MEGABONUS
          Сделать графическую оболочку для программы.

  */

  const Ingredients = [
    'Булка',
    'Огурчик',
    'Котлетка',
    'Бекон',
    'Рыбная котлета',
    'Соус карри',
    'Кисло-сладкий соус',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Капуста',
    'Кунжут',
    'Сыр Чеддер',
    'Сыр Виолла',
    'Сыр Гауда',
    'Майонез'
  ];

  var OurMenu = [];
  var OurOrders = [];
  let id = 0;

  const burgerProto = {
    ingredients: ['Булка', 'Кунжут'],
    showIngredients: function(){
      let {ingredients, name} = this;
      let ingredientsLength = ingredients.length;
      console.log(ingredientsLength);
      console.log(ingredientsLength);
      if( ingredientsLength !== 0){
        ingredients.map( item => {
            console.log( 'Состав бургера', name, item );
        })
      }
    }
  }

  function Burger( name, ingredients, cookingTime){
    this.name = name,
    this.ingredients = this.__proto__.ingredients.concat(ingredients),
    this.cookingTime = cookingTime

    OurMenu.push(this);
  }
  Burger.prototype = burgerProto;

  let burger = new Burger ('Hamburger', ['Огурчик', 'Котлетка'], 20);
  let burger2 = new Burger ('Cheeseburger', ['Булка', 'Огурчик', 'Котлетка', 'Сыр Чеддер', 'Кунжут'], 30);
  let burger3 = new Burger ('Fishburger', ['Булка', 'Рыбная котлета', 'Майонез', 'Помидорка'], 15);
  // console.log(burger);
  // console.log(burger.showIngredients() );
  // console.log( OurMenu );




//Order
  function Order(name, condition, value){
    id++
    this.id = id,
    this.orderNumber = id   

    if (name != ''){
      this.orderBurger = name;
        OurMenu.map (burger => {
          if (burger.name === name){
            console.log(`Your order NO: ${this.orderNumber}. ${burger.name} will be ready in ${burger.cookingTime} minutes`);
          }
        });
    }
    else if (condition != undefined) {
      function checkIngredient(elem) {
        return elem === value
      }

      if (condition === 'has'){
        this.orderAvailability = value;
        //Делаем массив из названий бургеров, чтобы предложить несколько, если критериям поиска соответствует несколько
        this.orderBurger = [];

        OurMenu.map(burger => {
            if (burger.ingredients.some(checkIngredient)){
              
              this.orderBurger.push(burger.name)
              console.log(`Your order NO: ${this.orderNumber}. We can offer you this burger: ${burger.name} cooking time is ${burger.cookingTime} minutes`);
            }
        });
      }
      else if (condition === 'except'){
        this.orderException = value;
        //Делаем массив из названий бургеров, чтобы предложить несколько, если критериям поиска соответствует несколько
        this.orderBurger = [];
          OurMenu.map(burger => {
            if (burger.ingredients.some(checkIngredient)){

              this.orderBurger.push(burger.name);              
              console.log(`Your order NO: ${this.orderNumber}. We can offer you this burger: ${burger.name} cooking time is ${burger.cookingTime} minutes`);
            }
          });
      }
      if (this.orderBurger.length === 0) {
          console.log(`Your order NO: ${this.orderNumber}. Unfortunately we couldn't find burger that corresponds to your request`)
      }
    }
  }

  let order1 = new Order ('Cheeseburger');
  console.log('order1', order1);
  let order2 = new Order ('','has', 'Огурчик');
  console.log('order2', order2);  
  let order3 = new Order ('','except', 'Помидорка');
  console.log('order3', order3);